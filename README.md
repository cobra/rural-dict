# 📖 Rural Dictionary

> We're rural, not urban.

Privacy-respecting, NoJS-supporting Urban Dictionary frontend.

## 🌐 Instances

| URL                                                                                                                                                                                 | Country | Owner name | Owner Website          |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------|------------|------------------------|
| <https://rd.vern.cc> + [Tor](http://rd.vernccvbvyi5qhfzyqengccj7lkove6bjot2xhh5kajhwvidqafczrad.onion) + [I2P](http://vern5cxiaufqvhv4hu5ypkvw3tiwvuinae4evdbqzrioql6s2sha.b32.i2p) | US      | ~vern      | <https://vern.cc>      |
| <https://rd.bloat.cat>                                                                                                                                                              | RO      | bloatcat   | <https://bloat.cat>    |
| <https://rd.thirtysix.pw>                                                                                                                                                           | NL      | thirtysix  | <https://thirtysix.pw> |

## ✨ Features

Frontend supports all Urban Dictionary features and has endpoint-parity with it.
Available features include:

- Word definitions
- Author pages
- Homepage with words of the day
- Random word definitions
- 404 page with words similar to search
- Pagination

## 🚀 Deployment

Clone repository:

```sh
git clone https://git.vern.cc/cobra/rural-dict.git
cd rural-dict
```

### 🐳 With Docker

```sh
docker build . -t rural-dict
docker compose up -d
```

### 💻 Without containerization

```sh
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements.lock
uvicorn src.main:app --no-access-log --proxy-headers --forwarded-allow-ips '*' --host 0.0.0.0 --port 5758
```

### 🛡️ Running behind a reverse proxy

To run the app behind a reverse proxy, ensure that the appropriate proxy headers are added.
Below is a sample configuration for NGINX:

```text
location / {
    proxy_pass http://127.0.0.1:5758;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
}
```

## 🔧 Development

Install Rye by following
the [installation guide](https://rye.astral.sh/guide/installation/).

Use `rye sync` to install dependencies and required Python version.

Use `rye run dev` to start development server which will reload on every change to source code.

Use `rye check --fix` and `rye fmt` to lint and format code. Assumed to be run before each commit
to guarantee code quality.

Use `rye run basedpyright` to ensure typing is correct.

## 🤝 Support

Join our [Matrix room](https://mto.vern.cc/#/#cobra-frontends:vern.cc) for support and other
things related to Rural Dictionary.

## 🔗 Redirection

To use Rural Dictionary, simply replace an Urban Dictionary URL with a Rural Dictionary URL from
the instance list above. Auto-redirect browser extension
like [Redirector](https://github.com/einaregilsson/Redirector) can be used to achieve this.

For example, change:

`https://urbandictionary.com/define.php?term=kin`

to:

`https://rd.vern.cc/define.php?term=kin`

**Note:** More endpoints are supported.

## 👥 Contributors

- [thirtysix](https://thirtysix.pw), rewrote project in a more modern libraries stack and
  implemented missing Urban Dictionary features
- [zortazert](https://codeberg.org/zortazert), created the initial Urban Dictionary frontend using
  JavaScript and helped develop Rural Dictionary

## 📜 License

This project is licensed under the AGPLv3+ license - see the [license file](LICENSE) for details.
